// Sven van Dam
// 10529772

d3.xml("test.svg", "image/svg+xml", function(error, xml) { 
	if (error) throw error; 

	document.body.appendChild(xml.documentElement);

	// width of svg element
	var w = window.innerWidth / 2;

	// select svg
	var svg = d3.select("svg")
		.attr("width", w)

	// data to be displayed
	var colors = ['#ccece6', '#99d8c9', '#66c2a4', '#41ae76', '#238b45', '#005824', '#f3f3f3'];
	var labels = ['100', '1000', '10000', '100000', '1000000', '10000000', "Unknown"];

	// number of rows
	var n = labels.length;

	// properties of colored field
	var color_width = d3.select(".st1").attr("width");
	var color_height = d3.select(".st1").attr("height");
	var color_x = d3.select(".st1").attr("x");

	// properties of text fields
	var label_width = d3.select(".st2").attr("width");
	var label_height = d3.select(".st2").attr("height");
	var label_x = parseInt(d3.select(".st2").attr("x"));

	// margin between rows
	var margin = 11;

	// y position of first row
	var y_base = parseFloat(d3.select(".st1").attr("y"));

	// y step to next row
	var y_offset = parseFloat(label_height) + margin;

	// margin of text
	var text_offset = 5;

	// select all color fields
	svg.selectAll(".st1")
		.data(colors)

		// reposition y and give color
		.attr("y", function(d, i) { return y_base + i * y_offset; })
		.style("fill", function(d) { return d; })

	// append rects
	.enter().append("rect")
		// set desired properties
		.attr("class", "st1")
		.attr("x", color_x)
		.attr("y", function(d, i) { return y_base + i * y_offset; })
		.attr("width", color_width)
		.attr("height", color_height)
		.attr("id", function(d, i) { return "kleur" + (i + 1); })
		.style("fill", function(d) { return d; });

	// select all text fields
	svg.selectAll(".st2")
		.data(labels)

		// reposition y
		.attr("y", function(d, i) { return y_base + i * y_offset; })
	// append rects	
	.enter().append("rect")

		// set desired properties
		.attr("class", "st2")
		.attr("x", label_x)
		.attr("y", function(d, i) { return y_base + i * y_offset; })
		.attr("width", label_width)
		.attr("height", label_height)
		.attr("id", function(d, i) { return "tekst" + (i + 1); });

	// get empty list
	svg.selectAll("g")
		.data(labels)

		// append text fields
		.enter().append("text")

			// set desired properties
			.attr("x", label_x + text_offset)
			.attr("y", function(d, i) { return label_height / 2 + y_base + i * y_offset; })
			.attr("width", label_width)
			.attr("height", label_height)
			.style("alignment-baseline", "middle")
			.style("font", "14px sans-serif")

			// set value
			.text(function(d) { return d; });

});