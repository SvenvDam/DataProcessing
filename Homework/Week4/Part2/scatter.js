// Sven van Dam
// 10529772

d3.csv("data.csv", function(data){

	// isolate data in arrays
	var locations = data.map(function(d) { return d.LOCATION; });
	var gdp = data.map(function(d) { return d.GDP; });
	var trade = data.map(function(d) { return d.TRADE / d.POPULATION; });
	var population = data.map(function(d) { return d.POPULATION; });
	var wage = data.map(function(d) { return d.WAGE; });

	// size of scatter relative to window
	var scale = 0.8;
	var offset  = 0.5 * (1 - scale);

	var w = window.innerWidth * scale;
	var h = window.innerHeight * scale;

	// format numbers to currency
	var money = d3.format("$,.2f");

	// set scales of variables
	// x and y scales are enlarged to prevent dots from hitting the axes

	// x-axis
	var x_domain_scales = [0.98, 1.02];
	var x = d3.scale.linear()
		.range([0, w])
		.domain([x_domain_scales[0] * Math.min(...wage), x_domain_scales[1] * Math.max(...wage)])

		// make sure ticks match axis
		.nice();

	// y-axis
	var y_domain_scales = [1.1, 1]
	var y = d3.scale.linear()
		.range([0, h])
		.domain([y_domain_scales[0] * Math.max(...gdp), y_domain_scales[1] * 0])
		.nice();

	// radius of dots
	// scaled to window size
	var radius_bounds = [0.0075, 0.045];
	var rad = d3.scale.pow().exponent(0.8)
		.range([radius_bounds[0] * h, radius_bounds[1] * h])
		.domain([Math.min(...population), Math.max(...population)])
		.nice();

	// colors of dots
	var gradient = ["FF0000", "FF3800", "FF7100", "FFAA00", "FFE200", "E2FF00", "A9FF00", "71FF00", "38FF00", "00FF00"];

	// function to determine color
	// grouped to reduce effect of outliers
	// benchmarked a minimal value to set middle at 0
	// this also catches outliers
	var color = d3.scale.quantile()
		.range(gradient)
		.domain([Math.min(...trade), -Math.min(...trade)]);

	// set tooltip
	var tip = d3.tip()
		.attr("class", "tip")
		.html(function(d) {
			return "<span><center><b>" + d.LOCATION + "</b></center></span></br>" +
			"<table>" +
			"<tr><td>GDP per capita</td><td>" + money(d.GDP) + "</td></tr>" +
			"<tr><td>Net trade per capita</td><td>" + money(d.TRADE / d.POPULATION) + "</td></tr>" +
			"<tr><td>Population</td><td>" + parseFloat(d.POPULATION).toFixed(2) + "m</td></tr>" +
			"<tr><td>Average wage</td><td>" + money(d.WAGE) + "</td></tr>" +
			"</table>"
		});

	// resize svg area
	var field = d3.select("svg")
		.attr("width", window.innerWidth)
		.attr("height", window.innerHeight);

	// translations are done in every group
	// this makes x and y positions relative to translations of all parent nodes

	// create group in svg
	var scatter = field.append("g")
		.attr("transform", "translate(" +
			(offset * window.innerWidth) + ", " + (offset * window.innerHeight) +
			")");

	// draw background
	scatter.append("rect")
		.attr("class", "background")
		.attr("width", w)
		.attr("height", h);

	// call tooltip
	scatter.call(tip);

	// draw circles
	scatter.selectAll("circle")
		.data(data)
		.enter().append("circle")
			.attr("x", offset * w)
			.attr("cx", function(d) { return x(d.WAGE); })
			.attr("cy", function(d) { return y(d.GDP); })
			.attr("r", function(d) { return rad(d.POPULATION); })
			.style("fill", function(d) { return color(d.TRADE / d.POPULATION); })

			// add hover functionality
			.on("mouseover", tip.show)
			.on("mouseout", tip.hide);

	// draw axes
	// offset for axis text
	var axis_label_offset = [35, -80];

	// x-axis
	var x_axis = d3.svg.axis()
		.scale(x)
		.orient("bottom");

	scatter.append("g")
		.attr("class", "axis")
		.attr("transform", "translate(0," + h + ")")
		.call(x_axis.tickFormat(money))
		.append("text")
			.attr("class", "title")
			.attr("x", w / 2)
			.attr("y", axis_label_offset[0])
			.text("Average Wage");

	// y-axis
	var y_axis = d3.svg.axis()
		.scale(y)
		.orient("left");

	scatter.append("g")
		.attr("class", "axis")
		.call(y_axis.tickFormat(money))
		.append("text")
			.attr("class", "title")
			.attr("transform", "rotate(270)")
			.attr("x", -h / 2)
			.attr("y", axis_label_offset[1])
			.text("GDP per Capita");

	// draw title
	scatter.append("text")
		.attr("class", "title")
		.attr("x", w / 2)
		.attr("y", -0.5 * offset * h)
		.text("Economical Data of OECD Countries (2012)");

	// draw title
	scatter.append("text")
		.attr("class", "subtitle")
		.attr("x", w / 2)
		.attr("y", -0.2 * offset * h)
		.text("Sven van Dam, 10529772");	

	// give source of data
	var source_offsets = 10;
	scatter.append("text")
		.attr("class", "source")
		.attr("x", w - source_offsets)
		.attr("y", h - source_offsets)
		.html("Source: <a href=https://data.oecd.org/ target=_blank >data.oecd.org</a>");

	// variables for legend layout, proportional to window size
	var blocksize = [0.05 * w, 0.035 * h];
	var legend_margin = 7;
	var legend_padding = 5;
	var block_spacing = 1;

	// draw legend for colors
	// make group
	var colorlegend = scatter.append("g")
		.attr("class", "colorlegend")
		.attr("transform", "translate(" +
			(w - legend_margin) +
			", " +
			legend_margin +
			")");

	// add blocks
	colorlegend.selectAll("rect")
		.data(gradient)
		.enter().append("rect")
			.attr("x", function(d, i) {
				return -(i + 1) * (blocksize[0] + block_spacing);
			})
			.attr("width", blocksize[0])
			.attr("height", blocksize[1])
			.style("fill", function(d) { return d; });

	// add labels
	colorlegend.selectAll("text")
		.data(gradient)
		.enter().append("text")
			.attr("x", function(d, i) {
				return -(i + 1) * (blocksize[0] + block_spacing);
			})
			.attr("y", blocksize[1] + legend_padding)
			.text(function(d, i) {

				// do not show last value since verythin from inf falls into that category
				if (i == gradient.length - 1) { return ""; };

				// calculate interval linked to color and return upper bound
				return money(color.invertExtent(d)[1]);
			});

	// add title
	colorlegend.append("text")
		.attr("class", "title")
		.attr("x", -gradient.length / 2 * blocksize[0])
		.attr("y", 2 * blocksize[1])
		.text("Net Trade per Capita");

	// draw legend for circle size
	var reference_values = [300, 100, 10];
	var circledata = [{"value": reference_values[0], "color": "lightgrey"}, {"value": reference_values[1], "color": "grey"}, {"value": reference_values[2], "color": "black"}];
	var circle_padding = 30;

	// create group
	var circlelegend = scatter.append("g")
		.attr("class", "circlelegend")
		.attr("transform", "translate(" +
			(legend_padding + rad(reference_values[0]) + circle_padding) +
			", " +
			(legend_padding + rad(reference_values[0])) +
			")");

	// add circles
	circlelegend.selectAll("circle")
		.data(circledata)
		.enter().append("circle")
			.attr("r", function(d) { return rad(d.value); })
			.style("fill", function(d) { return d.color; });

	// add labels
	circlelegend.selectAll("text")
		.data(circledata)
		.enter().append("text")
			.attr("x", rad(300))
			.attr("y", function(d) { return -rad(d.value); })
			.text(function(d) { return d.value; });

	// add lines
	circlelegend.selectAll("line")
		.data(circledata)
		.enter().append("svg:line")
			.attr("x2", rad(300))
			.attr("y1", function(d) { return -rad(d.value); })
			.attr("y2", function(d) { return -rad(d.value); });

	// add tile
	circlelegend.append("text")
		.attr("class", "title")
		.attr("y", rad(300) + legend_padding)
		.text("Population (mln)");

});