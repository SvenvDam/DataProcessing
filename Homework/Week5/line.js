// Sven van Dam
// 10529772


d3.json('data5.json', function(error, raw_data) {

    if (error) {
        return console.error(error);
    }
    // extract countries
    var countries = Object.keys(raw_data);
    // start with first country selected
    var selected = countries[0];

    // subset relevant data
    var data = raw_data[selected];

    // variables to be displayed
    var keys = Object.keys(data[0]);
    var x_var = "year";
    keys.splice(keys.indexOf(x_var), 1);

    // link variables to colors
    color_vals = ["orangered", "navy", "deepskyblue"];
    colors = {};
    for (var i = 0, n = keys.length; i < n; i++) {
        colors[keys[i]] = color_vals[i];
    }

    var percentage_range = [100, 0];

    // get years for y range
    var years = data.map(function(d) { return parseInt(d.year); });

    // size of scatter relative to window
    var scale = 0.8;
    var offset = 0.5 * (1 - scale);

    var w = window.innerWidth * scale;
    var h = window.innerHeight * scale;

    // x-axis
    var x = d3.scale.linear()
        .range([0, w])
        .domain([Math.min(...years), Math.max(...years)])
        // make sure ticks match axis
        .nice();

    // y-axis
    var y = d3.scale.linear()
        .range([0, h])
        .domain([percentage_range[0], percentage_range[1]])
        .nice();

    // convert to strings to percentages
    function percent(str) {
        num = parseFloat(str);
        return num.toFixed(2) + "%";
    }

    // resize svg area
    var field = d3.select("svg")
        .attr("width", window.innerWidth)
        .attr("height", window.innerHeight);

    // create group in svg
    var linegraph = field.append("g")
        .attr("transform", "translate(" +
            (offset * window.innerWidth) + ", " + (offset * window.innerHeight) +
            ")");

    // draw background
    linegraph.append("rect")
        .attr("class", "background")
        .attr("width", w)
        .attr("height", h);

    var selector_offset = "20px";

    // make selector for countries
    var selector = d3.select("body").append("select")
        .attr("id", "selector")
        .style("top", selector_offset)
        .style("left", selector_offset)
        .on("change", pick_country);

    // add options
    selector.selectAll("option")
        .data(countries)
        .enter().append("option")
            .attr("value", function(d) { return d; })
            .text(function(d) { return d; });

    // actions on new selection
    function pick_country() {
        // set new data
        val = d3.select("#selector").property("value");
        selected = val;
        data = raw_data[selected];
        // update lines
        for (var i = 0, n = keys.length; i < n; i++) {
            // set line on relevant variable
            line.y(function(d){
                return y(parseFloat(d[keys[i]]));
            });
            // update path
            linegraph.select("#"+keys[i])
                .transition()
                .duration(300)
                .attr("d", line(data));
        }
        // update title
        linegraph.select(".main_title")
            .text("Electricity production sources distribution in " + selected);
    }

    // draw axes
    // offset for axis text
    var axis_label_offset = [35, -50];

    // x-axis
    var x_axis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    linegraph.append("g")
        .attr("class", "axis")
        .attr("transform", "translate(0," + h + ")")
        .call(x_axis.tickFormat(d3.format("d")))
        .append("text")
            .attr("class", "title")
            .attr("x", w / 2)
            .attr("y", axis_label_offset[0])
            .text("Year");

    // y-axis
    var y_axis = d3.svg.axis()
        .scale(y)
        .orient("left");

    linegraph.append("g")
        .attr("class", "axis")
        .call(y_axis.tickFormat(function(d) { return d + "%"; }))
        .append("text")
            .attr("class", "title")
            .attr("transform", "rotate(270)")
            .attr("x", -h / 2)
            .attr("y", axis_label_offset[1])
            .text("% of total production");

    // line function
    var line = d3.svg.line()
        .x(function(d) {
            return x(parseInt(d[x_var]));
        });

    // area to track mouse position
    var tracker = field.append("g")
        .attr("transform", "translate(" +
            (offset * window.innerWidth) + ", " + (offset * window.innerHeight) +
            ")");

    // define tracking field
    tracker.append("rect")
        .attr("class", "window")
        .attr("width", w)
        .attr("height", h)
        .on("mouseover", function() { moving_info.style("display", null); })
        .on("mouseout", function() { moving_info.style("display", "none"); })
        .on("mousemove", track_mouse);


    // draw lines of all countries for one variable
    function make_line(variable) {
        line.y(function(d){
            return y(parseFloat(d[variable]));
        });

        linegroup.append("path")
            .attr("class", "info line")
            .attr("id", variable)
            .attr("stroke", colors[variable])
            .attr("d", function(d) {
                return line(data);
            });
    }

    // create group for lines
    var linegroup = linegraph.append("g")
        .data(data)
        .attr("class", "linegroup");

    // draw line for each variable
    for (var i = 0, n = keys.length; i < n; i++) {
        make_line(keys[i]);
    }

    // group for moving items
    var moving_info = tracker.append("g");

    // vertical line
    moving_info.append("svg:line")
        .attr("class", "vert_line")
        .attr("y1", y(percentage_range[0]))
        .attr("y2", y(percentage_range[1]));

    var text_offset = 15;

    // add percentages text
    moving_info.selectAll("text")
        .data(keys)
        .enter().append("text")
            .attr("class", "moving_text")
            .attr("id", function (d, i) {
                return "text-" + d;
            });

    // add dots
    var radius = 3;
    moving_info.selectAll("circle")
        .data(keys)
        .enter().append("circle")
        .attr("class", "dot")
        .attr("id", function (d) {
            return "dot-" + d;
        })
        .attr("r", radius);

    // add year text
    moving_info.append("text")
        .attr("class", "moving_text")
        .attr("id", "text-year");

    // calculate x position of left edge of bucket
    var bisectYear = d3.bisector(function(d) { return d.year; }).left;

    // define actions on move of mouse
    function track_mouse () {
        // calculate interval
        var x0 = x.invert(d3.mouse(this)[0]),
            i = bisectYear(data, x0, 1),
            d0 = data[i - 1],
            d1 = data[i],
            d = x0 - d0.year > d1.year - x0 ? d1 : d0;
        // set new x value for moving items
        moving_info.attr("transform", "translate(" + x(d.year) + ",0)");
        // update position and value of text and dots
        for (var j = 0, n = keys.length; j < n; j++) {
            var val = keys[j];
            moving_info.select("#text-" + val)
                .attr("y", y(d[val]) - text_offset)
                .text(percent(d[val]));
            moving_info.select("#dot-" + val)
                .attr("cy", y(d[val]));
        }
        // update year text
        moving_info.select("#text-year")
            .text(d.year);
    }

    // draw title
    var main_title_offset = -0.5;
    linegraph.append("text")
        .attr("class", "main_title")
        .attr("x", w / 2)
        .attr("y", main_title_offset * offset * h)
        .text("Electricity production sources distribution in " + selected);

    // draw title
    var subtitle_offset = -0.2;
    linegraph.append("text")
        .attr("class", "subtitle")
        .attr("x", w / 2)
        .attr("y", subtitle_offset * offset * h)
        .text("Sven van Dam, 10529772");

    // give source of data
    var source_offsets = 10;
    linegraph.append("text")
        .attr("class", "source")
        .attr("x", w - source_offsets)
        .attr("y", - source_offsets)
        .html("Source: <a href=http://data.worldbank.org/indicator/?tab=all target=_blank >The World Bank</a>");

    // create legend
    var legend_offset = 15;
    var legend = linegraph.selectAll(".legendblock")
        .data(keys)
        .enter().append("g")
            .attr("class", "legendblock")
            .attr("transform", "translate(" + legend_offset + ", " + legend_offset + ")");

    // add colored blocks
    var blocksize = 10;
    var block_padding = 2;
    var legendsize = 75;
    legend.append("rect")
        .attr("width", blocksize)
        .attr("height", blocksize)
        .attr("x", function(d, i) { return i * legendsize; })
        .attr("y", 0)
        .attr("fill", function (d) { return colors[d]; });

    // add labels
    legend.append("text")
            .attr("class", "legendtext")
            .attr("x", function(d, i) { return i * legendsize + blocksize + block_padding; })
            .text(function(d) { return d; });
});
