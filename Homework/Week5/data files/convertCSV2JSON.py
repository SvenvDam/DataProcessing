# Sven van Dam, 10529772

import csv
import json

# specific values
codes = ['CAN', 'FRA', 'GER', 'ITA', 'JPN', 'UK', 'USA']
names_in = [
    "can.csv", "fra.csv", "ger.csv", "ita.csv", "jpn.csv", "uk.csv", "usa.csv"]


delimiter = ','
merged = []

for i, filename_in in enumerate(names_in):
    # open and read csv file
    with open(filename_in, 'rb') as infile:
        reader = csv.DictReader(infile, delimiter=delimiter)
        data = list(reader)
        merged.append({
            'country': codes[i],
            'values': data
            })

# open and write to json
with open('data5.json', 'wb') as outfile:
    json.dump(merged, outfile)
