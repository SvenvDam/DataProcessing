# Sven van Dam, 10529772

import csv
import json

# specific values
names_in = []
names_out = []
for y in range(2000, 2015):
    names_in.append(str(y) + ".tsv")
    names_out.append(str(y) + ".json")

delimiter = '\t'

for i, filename_in in enumerate(names_in):
    # open and read csv file
    with open(filename_in, 'r') as infile:
        reader = csv.DictReader(infile, delimiter=delimiter)
        data = list(reader)
        out = {}
        for country in data:
            out[country["Code"]] = country

    # open and write to json
    with open(names_out[i], 'w') as outfile:
        json.dump(out, outfile)
