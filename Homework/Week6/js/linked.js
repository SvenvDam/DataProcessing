// Sven van Dam
// 10529772

// sources of energy
var energy_sources = ["Coal", "Oil", "Gas", "Nuclear", "Hydroelectric", "Renewable", "Remaining"],
    bardata, colors, update, countrydata;

// set color scheme for map
var map_colorSchemeSelect = "RdYlGn",
    map_colorScheme = colorbrewer[map_colorSchemeSelect],
    map_quantiles = 6,
    selected_color = "MidnightBlue",
    barcolors = ["#f3f3f3", "MidnightBlue"];

// make color scale
var color = d3.scale.threshold()
    .range(map_colorScheme[map_quantiles]);

// set color scheme for map
var bar_colorSchemeSelect = "RdBu",
    bar_colorScheme = colorbrewer[bar_colorSchemeSelect],
    bar_quantiles = 7;

// scale for size of bar
var minmax_bar = [0, 70],
    minmax_percentage = [0,100];

// colors of bars
var bar_color = d3.scale.linear()
    .range(barcolors)
    .domain(minmax_percentage);

var barsize = d3.scale.linear()
    .range(minmax_bar)
    .domain(minmax_percentage);

// make queue of all files
queue()
    .defer(d3.json, "data/2000.json")
    .defer(d3.json, "data/2001.json")
    .defer(d3.json, "data/2002.json")
    .defer(d3.json, "data/2003.json")
    .defer(d3.json, "data/2004.json")
    .defer(d3.json, "data/2005.json")
    .defer(d3.json, "data/2006.json")
    .defer(d3.json, "data/2007.json")
    .defer(d3.json, "data/2008.json")
    .defer(d3.json, "data/2009.json")
    .defer(d3.json, "data/2010.json")
    .defer(d3.json, "data/2011.json")
    .defer(d3.json, "data/2012.json")
    .defer(d3.json, "data/2013.json")
    .defer(d3.json, "data/2014.json")
    .awaitAll(ready);

// load all data files
function ready(error, data) {

    // handle errors
    if (error) throw error;

    // start with lowest year
    var current = data[0];

    // countries and years from data
    var codes = Object.keys(current),
        years = [],
        keys = {};

    // set initial country
    var selected = codes[0];

    // read all years
    $.each(data, function(i, val) {
        years.push(val[codes[0]].Time);
        // track indexes
        keys[years[i]] = i;
    });

    // extract all consumption numbers
    var consumption = [];
    $.each(data, function(i, year) {
        $.each(codes, function(j, country) {
            consumption.push(parseFloat(year[country].Consumption));
        });
    });

    // upper and lower bound of consumption range
    var rounding_factor = 100,
        upper = Math.ceil(Math.max(...consumption) / rounding_factor) * rounding_factor,
        lower = Math.floor(Math.min(...consumption) / rounding_factor) * rounding_factor;

    // calcultate thresholds for colors
    var domain = _.range(lower, upper, Math.round((upper - lower) / map_quantiles));

    // round values to 100
    $.each(domain, function(i, val) {
        domain[i] = Math.round(val / rounding_factor) * rounding_factor;
    });

    // remove first item from domain
    // otherwise everything below the lowes value would be assigned the 'lowest' color
    domain.shift();

    // set domain of color scale
    color.domain(domain);

    // make object of intervals and colors for legend
    var buckets = {};
    $.each(color.range().reverse(), function(i, hex) {
        var interval = color.invertExtent(hex);
        buckets[
            "(" + ((Math.round(interval[0] / 100) * 100) || 0) +
            ", " +
            (Math.round(interval[1] / 100) * 100) +
            "]"] = hex;
    });

    // default color for countries
    buckets["defaultFill"] = "white";

    // styllistic variables
    var barheight = 25,
        bar_spacing = 10,
        transition_time = 150,
        bar_offsets = [110, 60],
        bar_text_offset = 1;

    // actions to set visualisation
    function set_page() {

        // get year
        year = d3.select("#slider").property("value");

        // update text under slider
        yeartext.text("Year: " + year);

        // update data
        current = data[keys[year]];

        // make object of counries and colors
        colors = {};
        $.each(codes, function(i, val) {
            colors[val] = color(current[val].Consumption);
        });

        // update map
        map.updateChoropleth(colors);

        // keep selected country in highlighted color
        color_selected();

        // redraw bars
        draw_bar();
    }

    // make selected country red
    function color_selected() {
        update =  {};
        update[selected] = selected_color;
        console.log(selected);
        map.updateChoropleth(update);
    }

    // procedure to draw bars
    function draw_bar(bardata) {

        // get data of country in selected year
        countrydata = current[selected];

        // store value of each source in object
        bardata = [];
        $.each(energy_sources, function(i, val) {
            bardata.push({
                'source': val,
                'value': parseFloat(countrydata[val])
            });
        });

        // set size of bar scaled to size of div
        bar.selectAll(".bar rect")
            .data(bardata)
            .transition().duration(transition_time)
            .attr("width", function(d) {
                return barsize(d.value) + "%";
            })

            // set color
            .attr("fill", function(d) {
                return bar_color(d.value);
            });

        // update text on end of bars
        bar.selectAll(".bartext")
            .data(bardata)
            .transition().duration(transition_time)
            .attr("x", function(d) {
                return (barsize(d.value) + bar_text_offset) + "%";
            })
            .text(function(d) {
                return percent(d.value);
            });

        // update subtitle
        bar.select(".countryname")
            .text("Distribution of energy production in " + countrydata.Name + ", " + countrydata.Time);

        // update consumption text beneath bars
        bar.select(".consumption")
            .text("kWh per capita: " + Math.round(countrydata.Consumption));
    }

    // convert to strings to percentages
    function percent(str) {
        num = parseFloat(str);
        return num.toFixed(2) + "%";
    }

    // set slider values to years in data
    d3.select("#slider")
        .attr("min", Math.min(...years))
        .attr("max", Math.max(...years))
        .attr("value", Math.min(...years))
        .on("input", set_page);

    // append text under slider
    var yeartext = d3.select("#yeartext");

    // field of bars
    var field = d3.select("#container2").append("svg")
        .attr("width", "100%")
        .attr("height", "100%");

    // create group
    var bar = field.append("g")
        .attr("id", "bar");

    // make map with datamaps library
    var map = new Datamap({

        // select div
        element: $('#container')[0],
        setProjection: function(element) {

            // zoom on europe
            var projection = d3.geo.equirectangular()
                .center([20, 40])
                .rotate([10, -15])
                .scale(750)
                .translate([element.offsetWidth / 2, element.offsetHeight / 2]);
            var path = d3.geo.path()
                .projection(projection);
            return {path: path, projection: projection};
        },

        // set fill colors and add label
        fills: buckets,
        legend: true,
        // supress hover color
        geographyConfig: {
            highlightOnHover: false,

            // define what to show on hover
            popupTemplate: function(geography) {

                // only show popup for countries in dataset
                var hover_country = geography.properties.iso;
                if ($.inArray(hover_country, codes) != -1) {

                    // show country name and consumtion
                    return "<table class=popup bgcolor=\"red\">" +
                    "<tr>" +
                    "<td class=popup-col>" + geography.properties.name + "</td>" +
                    "<td class=popup-col>" + Math.round(current[hover_country].Consumption) + " kWh</td>" +
                    "</tr>" +
                    "</table>";
                }
            }
        },

        // define action when country is clicked
        done: function(datamap) {
            datamap.svg.selectAll('.datamaps-subunit').on('click', function(geography) {
                selected = geography.properties.iso;

                // check if country in dataset
                if ($.inArray(selected, codes) != -1){

                    // set correct year and draw bars
                    set_page();
                }
            });
        }
    });

    // set legend title
    map.legend({legendTitle : "Energy consumption, kWh per capita"});

    // create bars
    var barchart = bar.selectAll(".bar")
        .data(energy_sources)
        .enter().append("g")
            .attr("transform", "translate(" + bar_offsets[0] + ", " + bar_offsets[1] + ")")
            .attr("class", "bar")
            .append("rect")
                .attr("height", barheight)
                .attr("y", function(d, i) {
                    return i * (barheight + bar_spacing);
                });

    // create text at end of bars
    bar.selectAll(".bar").append("text")
        .data(energy_sources)
        .attr("class", "bartext")
        .attr("y", function(d, i) {
            return i * (barheight + bar_spacing) + barheight / 2;
        });

    // create and draw labels
    bar.selectAll(".bar")
        .data(energy_sources)
        .append("text")
        .attr("class", "sourcetext")
        .attr("x", -bar_spacing)
        .attr("y", function(d, i) {
            return i * (barheight + bar_spacing) + barheight / 2;
        })
        .text(function(d) { return d; });

    // title of bars
    bar.append("text")
        .attr("class", "countryname")
        .attr("x", barsize(50) + "%")
        .attr("y", barheight + bar_spacing);

    // create text displaying energy consumption below bars
    bar.append("text")
        .attr("class", "consumption")
        .attr("x", barsize(50) + "%")
        .attr("y", (energy_sources.length + 2) * (barheight + bar_spacing));

    // give source of data
    bar.append("text")
        .attr("class", "source")
        .attr("y", "100%")
        .html("Source: <a href=http://data.worldbank.org/indicator/?tab=all target=_blank >The World Bank</a>");

    // link to story
    bar.append("text")
        .attr("class", "story")
        .attr("x", barsize(50) + "%")
        .attr("y", "90%")
        .html("<a href=story.html>The story</a>");

    // initialise page
    set_page();

}
