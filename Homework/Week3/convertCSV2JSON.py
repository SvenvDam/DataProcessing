# Sven van Dam, 10529772

import csv
import json

# specific values
filename_in  = 'data_formatted.csv'
filename_out = 'data.json'
delimiter    = ';'

# open and read csv file
with open(filename_in, 'rb') as infile:
	reader = csv.DictReader(infile, delimiter = delimiter)
	data   = list(reader)

# open and write to json
with open(filename_out, 'wb') as outfile:
	json.dump(data, outfile)