// Sven van Dam, 10529772

// set margins and size
var margin = {top: 50, right: 30, bottom: 20, left: 75},
    width  = 900 - margin.left - margin.right,
    height = 500 - margin.top - margin.bottom;

// load data and act upon completion
d3.json("data.json", function(data) {

	// set function to calculate x and y positions
	var x = d3.scale.ordinal()
    	.rangeRoundBands([0, width], .1);

	var y = d3.scale.linear()
    	.range([height, 0]);

    // set x domain
	x.domain(data.map(function(d) { return d.month; }));

	// set values in array
	var temp = data.map(function(d) { return d.rainfall});

	// set y domain
    y.domain([0, Math.max(...temp)]);

    // axes
    var x_axis = d3.svg.axis()
    	.scale(x)
    	.orient("bottom");

    var y_axis = d3.svg.axis()
    	.scale(y)
    	.orient("left");

    // info block
    var tip = d3.tip()
    	.attr("class", "tip")
    	.html(function(d) {
    		return "<span>" + d.rainfall + " mm</span>"
    	});

    // select the cart object and set the general area for the chart
	var chart = d3.select(".barchart")
    	.attr("width", width + margin.left + margin.right)
    	.attr("height", height + margin.top + margin.bottom)
    .append("g")
    	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    // add info blocks
    chart.call(tip);

    // add x and y axis
    chart.append("g")
    	.attr("class", "axis")
    	.attr("transform", "translate(0," + height + ")")
    	.call(x_axis);

    chart.append("g")
    	.attr("class", "axis")
      	.call(y_axis)
      	.append("text")
      		.attr("dy", "-20px")
      		.attr("dx", "-25px")
      		.text("mm");

    // title
    chart.append("text")
    	.attr("class", "title")
    	.attr("x", width / 2)
    	.text("Monthly rainfall in de Bilt (2015)");


    // add the bars
	var bar = chart.selectAll(".bar")
    	.data(data)
    .enter().append("rect")
    	.attr("class", "bar")
      	.attr("x", function(d) { return x(d.month); })
      	.attr("y", function(d) { return y(d.rainfall); })
      	.attr("height", function(d) { return height - y(d.rainfall); })
      	.attr("width", x.rangeBand() * 0.75)
      	.on("mouseover", tip.show)
      	.on("mouseout", tip.hide);
});
