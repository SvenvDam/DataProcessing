import csv

filename_in = 'data.csv'
filename_out = 'data_formatted.csv'
delimiter = ';'
month_vals = [0] * 12
month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec']



# open and read csv file
with open(filename_in, 'rb') as infile:
	reader = csv.DictReader(infile, delimiter = delimiter)
	data = list(reader)

for row in data:
	row['month'] = int(row['date'][4:6])

	if int(row['rainfall']) == -1:
		row['rainfall'] = 0

	month_vals[row['month'] - 1] += int(row['rainfall'])

with open(filename_out, 'wb') as outfile:
	writer = csv.writer(outfile, delimiter = delimiter)
	writer.writerow(['month', 'rainfall'])

	for i in range(len(month_vals)):
		writer.writerow([month_names[i], month_vals[i]])



