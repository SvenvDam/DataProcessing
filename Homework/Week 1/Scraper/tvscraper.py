#!/usr/bin/env python
# Name: Sven van Dam
# Student number: 10529772
'''
This script scrapes IMDB and outputs a CSV file with highest rated tv series.
'''
import csv
import re

from pattern.web import URL, DOM

TARGET_URL = "http://www.imdb.com/search/title?num_votes=5000,&sort=user_rating,desc&start=1&title_type=tv_series"
BACKUP_HTML = 'tvseries.html'
OUTPUT_CSV = 'tvseries.csv'
INFO = ['Title', 'Rating', 'Genre', 'Actors', 'Runtime']

def error_handler(item, command):
	'''
	Handles missing values
	'''	
	if not item(command):
		return "Missing"
	return item(command)	

def extract_tvseries(dom):
    '''
    Extract a list of highest rated TV series from DOM (of IMDB page).

    Each TV series entry should contain the following fields:
    - TV Title
    - Rating
    - Genres (comma separated if more than one)
    - Actors/actresses (comma separated if more than one)
    - Runtime (only a number!)
    '''

    # list to be returned
    series_list = []

    # for each series
    for item in dom.body.by_class("lister-item"):

        # retrieve info from item in dom using CSS selectors
        vals = [

        # title
        error_handler(item, "h3.lister-item-header a")[0].content,

        # rating
        error_handler(item, "div.ratings-imdb-rating strong")[0].content,

        # genre
        error_handler(item, "span.genre")[0].content.strip(),

        # actors, list converted to ','-separated string
        ", ".join([actor.content for actor in error_handler(item, "div.lister-item-content p a")]),

        # runtime
        error_handler(item, "span.runtime")[0].content
        ]

        # merge keys and values to dict
        merged = dict(zip(INFO, vals))

        # append dict to list
        series_list.append(merged)

    # return list
    return series_list    


def save_csv(f, tvseries):
    '''
    Output a CSV file containing highest rated TV-series.
    '''
    writer = csv.writer(f)

    # write header
    writer.writerow(INFO)

    # write utf-8 encoded text to csv
    for row in tvseries:
        writer.writerow([row[item].encode("UTF-8") for item in INFO])

if __name__ == '__main__':
    # Download the HTML file
    url = URL(TARGET_URL)
    html = url.download()

    # Save a copy to disk in the current directory, this serves as an backup
    # of the original HTML, will be used in grading.
    with open(BACKUP_HTML, 'wb') as f:
        f.write(html)

    # Parse the HTML file into a DOM representation
    dom = DOM(html)

    # Extract the tv series (using the function you implemented)
    tvseries = extract_tvseries(dom)

    # Write the CSV file to disk (including a header)
    with open(OUTPUT_CSV, 'wb') as output_file:
        save_csv(output_file, tvseries)